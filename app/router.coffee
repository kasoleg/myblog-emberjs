App = require 'app'


App.Router.map ->
  # put your routes here
  @route "index",
    path: "/"

  @route "post",
    path: "/posts/:post_id"
  ->
  @route "addPost"
  @route "editPost",
    path: "editPosts/:post_id"
 
  @resource "blog",
    path: "blog" 
  ,  ->
    @route "posts",
      path: "posts",
      queryParams: ['page']
    @route "post",
      path: "posts/:post_id"
    @route "addPost",
      path: "posts/addPost"
    @route "editPost",
      path: "posts/editPost/:post_id"
  return