App = require 'app'

App.EditPostController = Ember.ObjectController.extend(
  actions:
    savePost: ->
      @get("model").save
      
      @transitionToRoute "index"
      
      return
)