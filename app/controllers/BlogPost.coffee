App = require 'app'

App.BlogPostController = Ember.ObjectController.extend
  sinopsis: (->
    str = "..."
    if (@get "description").length > 30
      (@get "description").substring(0, 30) + str
    else
      (@get "description").substring(0, 30)
  ).property()