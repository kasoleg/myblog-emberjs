App = require 'app'

App.BlogAddPostController = Ember.ObjectController.extend(
  content: {}
  actions:
    savePost: ->
      newPost = @store.createRecord("post",
        title: @get("title")
        description: @get("description")
      )

      newPost.save()
      @transitionToRoute "index"
      
      return
)
