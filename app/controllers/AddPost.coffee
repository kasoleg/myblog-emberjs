App = require 'app'

App.AddPostController = Ember.ObjectController.extend(
  content: {}
  actions:
    savePost: ->
      newPost = @store.createRecord("post",
        title: @get("title")
        description: @get("description")
      )

      @transitionToRoute("index")
      return
)
