App = require 'app'

App.BlogPostsController = Ember.ArrayController.extend
  itemController: "blogPost" 
  actions:
    deletePost: (post) ->
      if confirm('You want to delete post ' + post._data.title + '. Are you sure?')
        post.destroyRecord()
      return

    numberOfPage: (number) ->
      @transitionToRoute 'blog.posts',
        queryParams:
          page: number
      return

  
    #  @get('model.content')[i]._data.description.substring(0, 30) + "..."
    
