App = require 'app'

module.exports = App.BlogPostsRoute = Ember.Route.extend
  model: ->
    @store.find "post", page: arguments[0].queryParams.page

  setupController: (controller, model) ->
    @_super arguments...
    console.log model

    controller.set 'routeName', @routeName
    Em.$.ajax(
      url: "/getCountOfPages"
    ).done (data) ->
      controller.set "pages", data 
      return
    return

  actions: 
    queryParamsDidChange: (params) ->
      params.page
      @refresh()