App = require 'app'

module.exports = App.IndexRoute = Ember.Route.extend
  model: ->
    data = @store.find "post"

  redirect: ->
    @transitionTo "blog.posts",
      queryParams:
        page: 1
        