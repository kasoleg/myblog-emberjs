express = require("express")
path = require("path") #модуль для парсинга пути
log = require("./libs/log")(module)
config = require("./libs/config")
log = require("./libs/log")(module)
PostModel = require("./libs/mongoose").PostModel
bodyParser = require("body-parser")

app = express()

#app.use express.favicon() # отдаем стандартную фавиконку, можем здесь же свою задать
#app.use express.logger("dev") # выводим все запросы со статусами в консоль
app.use bodyParser() # стандартный модуль, для парсинга JSON в запросах
#app.use express.methodOverride() # поддержка put и delete
#app.use app.router # модуль для простого задания обработчиков путей
#app.use express.static(path.join(__dirname, "public")) # запуск статического файлового сервера, который смотрит на папку public/ (в нашем случае отдает index.html)

#middleware for CORS 
allowCrossDomain = (req, res, next) -> 
  res.header "Access-Control-Allow-Origin", "*" 
  res.header "Access-Control-Allow-Methods", "GET,PUT,POST,DELETE" 
  res.header "Access-Control-Allow-Headers", "Content-Type" 
  next() 
  return

app.use allowCrossDomain

app.get "/blog/posts", (req, res) ->
  console.log req.query.page
  #PostModel.find (err, posts) ->
  PostModel.find({}).skip((req.query.page - 1) * 10).limit(10).exec((err, posts) ->
    unless err
      #for i in [0...posts.length]
        #str = posts[i].description
        #posts[i].description = str.substring(0, 30) + "..." 
      res.send 200,
        posts: posts
    else
      res.statusCode = 500
      log.error "Internal error(%d): %s", res.statusCode, err.message
      res.send error: "Server error"
    return)
  return 

app.post "/blog/posts", (req, res) ->
  console.log req.body
  post = new PostModel(
    title: req.body.post.title
    description: req.body.post.description
  )
  console.log post
  post.save (err) ->
    unless err
      log.info "post created"
      res.send post: post
    else
      console.log err
      if err.name is "ValidationError"
        res.statusCode = 400
        res.send error: "Validation error"
      else
        res.statusCode = 500
        res.send error: "Server error"
      log.error "Internal error(%d): %s", res.statusCode, err.message
    return
  return

app.get "/blog/posts/:id", (req, res) ->
  PostModel.findById req.params.id, (err, post) ->
    unless post
      res.statusCode = 404
      return res.send(error: "Not found")
    unless err
      res.send
        status: "OK"
        post: post

    else
      res.statusCode = 500
      log.error "Internal error(%d): %s", res.statusCode, err.message
      res.send error: "Server error"

app.put "/blog/posts/:id", (req, res) ->
  PostModel.findById req.params.id, (err, post) ->
    unless post
      res.statusCode = 404
      return res.send(error: "Not found")
    console.log post
    post.title = req.body.post.title
    post.description = req.body.post.description
    post.save (err) ->
      unless err
        log.info "post updated"
        res.send
          post: post

      else
        if err.name is "ValidationError"
          res.statusCode = 400
          res.send error: "Validation error"
        else
          res.statusCode = 500
          res.send error: "Server error"
        log.error "Internal error(%d): %s", res.statusCode, err.message
      return

app.delete "/blog/posts/:id", (req, res) ->
  PostModel.findById req.params.id, (err, post) ->
    unless post
      res.statusCode = 404
      res.send error: "Not found"
    post.remove (err) ->
      unless err
        log.info "post removed"
      else
        res.statusCode = 500
        log.error "Internal error(%d): %s",res.statusCode,err.message
        res.send error: "Server error"
      return
    return
  return

app.get "/getCountOfPages", (req, res) ->
  pagesArray = []
  pageSize = 10
  Math.ceil PostModel.count (err, count) ->
    pages = count / pageSize
    console.log pages
    for i in [0...pages]
      pagesArray.push i + 1
    return res.json pagesArray

console.log __dirname
app.use express.static(path.join(__dirname, "/../../public"))

app.listen(config.get("port"), ->
    console.log "Express server listening on port " + config.get("port")
    return
)

#обработка ошибок 404 и 500
app.use (req, res, next) ->
  res.status 404
  log.debug "Not found URL: %s", req.url
  res.send error: "Not found"
  return

app.use (err, req, res, next) ->
  res.status err.status or 500
  log.error "Internal error(%d): %s", res.statusCode, err.message
  res.send error: err.message
  return

app.get "/ErrorExample", (req, res, next) ->
  next new Error("Random error!")
  return
#------------------------------------------------------------------



